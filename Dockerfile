# Используем базовый образ Python 3.8
FROM python:3.8

# Устанавливаем рабочую директорию внутри контейнера
WORKDIR /app

# Копируем файл requirements.txt в контейнер
COPY requirements.txt .

# Устанавливаем зависимости из файла requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

# Копируем все файлы из текущей директории в контейнер
COPY . .

EXPOSE 8080

# Определяем команду, которая будет выполнена при запуске контейнера
CMD ["python", "app.py"]
CMD ["pytest", "test_app.py"]

