import json
from module import *




# +
@validate_params()
async def addition(request,a,b):
    return web.json_response({"result": a + b})
# -
@validate_params()
async def subtraction(request,a,b):
    return web.json_response({"result": a - b})
# *
@validate_params()
async def multiplication(request,a,b):
    return web.json_response({"result": a * b})
# /
@validate_params()
async def division(request,a,b):
    if b != 0:
        return web.json_response({"result": a / b})
    else:
        error_message = f"division by zero"
        return web.json_response({'error': error_message}, status=400)

    
app = web.Application()
app.add_routes([web.post('/addition', addition),
                web.post('/subtraction', subtraction),
                web.post('/multiplication', multiplication),
                web.post('/division', division),
                
                ])

if __name__ == '__main__':
    web.run_app(app)