
import requests


def send_net(urls, param1, param2):
    url = f"http://localhost:8080/{urls}"
    data = {"a": float(param1), "b": float(param2)}
    response = requests.post(url, json=data)

    if response.status_code == 200:
        response_data = response.json()
        return response_data
    else:
        return f"Error: {response.status_code} {response.text}"

def test_addition():
    assert send_net("addition", 1, 1) == {"result": 2}

def test_subtraction():
    assert send_net("subtraction", 1, 1) == {"result": 0}

def test_multiplication():
    assert send_net("multiplication", 1, 1) == {"result": 1}

def test_division():
    assert send_net("division", 1, 1) == {"result": 1}