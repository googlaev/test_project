from aiohttp import web
def validate_params():
    def decorator(func):
        async def wrapper(request):  
            try:
                data = await request.json()
                if not(('a' in data) and ('b' in data)):
                    print("ошибка колисетва параметров")
                    error_message = f"error arguments {data}"
                    return web.json_response({'error': error_message}, status=400)
                request_sril = {}
                for key in data:
                    request_sril[key]= float(data[key])
                    
            except:
                error_message = f"not correct data"
                return web.json_response({'error': error_message}, status=400)
            return await func(request,data["a"],data["b"])
        return wrapper
    return decorator

